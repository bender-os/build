#!/bin/bash
set -xeuo pipefail

# set proper umask
umask "${ETCD_BACKUP_UMASK}"

# make dirname and cleanup paths
BACKUP_FOLDER="$( date "${ETCD_BACKUP_DIRNAME}")" || { echo "Invalid backup dirname" && exit 1; }
BACKUP_PATH="${ETCD_BACKUP_SUBDIR}/${BACKUP_FOLDER}"
BACKUP_PATH_POD="/backup/${BACKUP_PATH}"
BACKUP_ROOTPATH="/backup/${ETCD_BACKUP_SUBDIR}"

# make nescesary directorys
mkdir -p "/host/var/tmp/etcd-backup"
mkdir -p "${BACKUP_PATH_POD}"

# create backup to temporary location
chroot /host /usr/local/bin/cluster-backup.sh /var/tmp/etcd-backup

# Next steps ... 

# Store backup in external location 

echo "Store backup in external location 👌👌 "